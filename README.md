# MobileInsight

Primary goals of the server: 

- Upload logfiles
- Archive logfiles
- Retrieve logfiles


Connect to Lightsail
https://lightsail.aws.amazon.com

Piublic IP 54.196.3.31

    > ssh -i LightsailDefaultPrivateKey.pem ubuntu@54.196.3.31

## Python web server

Framework python Bottle

    from bottle import static_file
    @route('/static/<filename>')
    def server_static(filename):
        return static_file(filename, root='/path/to/your/static/files')


https://bottlepy.org/docs/dev/tutorial.html

https://code.tutsplus.com/tutorials/creating-a-web-app-from-scratch-using-python-flask-and-mysql--cms-22972

https://realpython.com/blog/python/introduction-to-flask-part-1-setting-up-a-static-site/

## Bootstrap interface

Upload multiple files
https://github.com/blueimp/jQuery-File-Upload

http://bootstrap-table.wenzhixin.net.cn/documentation/#table-options

Tree to show the data
http://mbraak.github.io/jqTree/#tree-options


## Database

All the logs are stored in a MongoDB database hosted on the server

Connect to mongoDB

	$ mongo
	MongoDB shell version: 3.2.11
	connecting to: test
	> 

Random commands:

	> show dbs
	> show collections
	> use <dbname>
	> db.dropDatabase()
	> db.<dbase>.<collection>.find()
	> db.local.messages.drop()

# Mac

Adresse IP
82.236.68.108
https://baron9.freeboxos.fr:48246

port acces distant HTTP : 12896
ssh ben@82.236.68.108 -p 2222

Launch mongoDB service:
	
	brew install mongodb
	brew services [start|stop] mongodb
	sudo pip install pymongo

Launch web server

	git clone https://gitlab.com/bbaron/mobileInsight.git
	tar xvf MobileInsight-2.1.0.tar.gz
	cd MobileInsight-2.1.0
	sudo python setup.py install


	screen
	screen -ls 
	screen -r <screen_id>

Remove System Integrity Protection on OS X El Capitain

1. Restart while pressing CMD + R
2. Utilities > Terminal
3. Command `csrutil disable`
4. Close terminal and restart

# AWS

Connect with AWS
Console : https://us-west-2.console.aws.amazon.com

	chmod 400 MobileInsight.pem
	ssh -i "MobileInsight.pem" ubuntu@ec2-54-70-147-159.us-west-2.compute.amazonaws.com

Note that you may have to edit the permissions of the security group in the AWS dashboard


Install `MongoDB` on AWS Ubuntu instance https://docs.mongodb.com/v3.0/tutorial/install-mongodb-on-ubuntu/
https://askubuntu.com/questions/770054/mongodb-3-2-doesnt-start-on-lubuntu-16-04-lts-as-a-service
https://www.digitalocean.com/community/tutorials/how-to-install-mongodb-on-ubuntu-16-04



Install `pip` on AWS Ubuntu instance

	sudo apt install python-pip
	
Install `pymongo` 

	sudo pip install pymongo
	
Install `flask` 

	sudo pip install flask
	sudo pip install flask_mail

Clone the repository on the instance

    git clone https://gitlab.com/bbaron/mobileInsight

Launch the service on the AWS instance

	screen
	python app.py

MobileInsight install

	sudo pip install pyserial
	sudo pip install crcmod
	sudo pip install xmltodict
	sudo apt-get install wireshark

	tar -zxvf MobileInsight-2.1.0.tar.gz
	cd MobileInsight-2.1.0
	sudo python setup.py install

