from flask import Flask, render_template
from pymongo import MongoClient
from urllib.parse import quote_plus
import socket
import requests
import json
import boto3

servicediscovery = boto3.client("servicediscovery")

uri = "mongodb://%s:%s@%s" % (
    quote_plus("user"), quote_plus("password"), "mongo.local:27017")
client = MongoClient(uri)
db = client['mobileInsight']

app = Flask(__name__)

@app.route("/")
def hello():
    return "Hello World from Mobile Insights API with the new version"

@app.route("/test")
def test():
    app.logger.info('testing')
    collection = db['test']
    post = {"author": "Mike",
            "text": "My first blog post!",
            "tags": ["mongodb", "python", "pymongo"]}

    post_id = collection.insert_one(post).inserted_id
    app.logger.info('inserted document %s', post_id)
    
@app.route("/test2")
def test2():
    resp = servicediscovery.list_services()
    service_obj = [service for service in resp.get('Services', [])]
    services = []
    for service in service_obj:
        services.append({
            'name': service['Name'],
            'addr': socket.gethostbyname(service['Name']+".local"),
            'num_hosts': len(servicediscovery.list_instances(ServiceId=service['Id'])['Instances']),
        })
    return json.dumps(services)

@app.route("/worker")
def worker():
    return requests.get("http://worker.local:8080").content.decode("utf-8")


@app.route("/worker80")
def worker80():
    return requests.get("http://worker80.local").content.decode("utf-8")


if __name__ == "__main__":
    # Only for debugging while developing
    app.run(host='0.0.0.0', debug=True, port=80)
