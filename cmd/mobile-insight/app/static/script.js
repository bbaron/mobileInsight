$(document).ready(function () {

var withLiCreate = function(node, $li) {
    if('ts' in node && 'type' in node) {
        $li.find('.jqtree-element').append(
            " " + node.ts + " " + node.type
        );
    } else if('value' in node) {
        $li.find('.jqtree-element').append(
            " <b>" + node.value + "</b>"
        );
    }

    var title = $li.find('.jqtree-title'),
    search = filter.val().toLowerCase(),
    value = title.text().toLowerCase();

    // console.log("\tvalue: "+value+" | search: "+search);

    if(search !== ''){
        $li.hide();
        if(value.indexOf(search) > -1) {
            $li.show();
            var parent = node.parent;
            while(typeof(parent.element) !== 'undefined') {
                $(parent.element)
                    .show()
                    .addClass('jqtree-filtered');
                parent = parent.parent;
            }
        }
        if(!filtering) {
            filtering = true;
        };
        if(!tree.hasClass('jqtree-filtering')) {
            tree.addClass('jqtree-filtering');
        };
    } else {
        if(filtering) {
            filtering = false;
        };
        if(tree.hasClass('jqtree-filtering')) {
            tree.removeClass('jqtree-filtering');
        };
    };

}

var tree = $('#tree'),
    filter = $('#filter'),
    filtering = false,
    thread = null;

tree.tree({
    useContextMenu: false,
    onCreateLi: function(node, $li) {
        withLiCreate(node, $li);
    },
    onCanSelectNode: function (node) {
        if (node.children.length == 0) {
            // Nodes without children can be selected
            return true;
        }
        else {
            // Nodes with children cannot be selected
            return false;
        }
    }
});

filter.keyup(function() {
    clearTimeout(thread);
    state = tree.tree('getState');
    console.log(state);
    thread = setTimeout(function () {
        tree.tree('setState', state);
        tree.tree({
            useContextMenu: false,
            onCreateLi: function(node, $li) {
                withLiCreate(node, $li);
            },
            onCanSelectNode: function (node) {
                if (node.children.length == 0) {
                    // Nodes without children can be selected
                    return true;
                }
                else {
                    // Nodes with children cannot be selected
                    return false;
                }
            }
        })
    }, 50);
});

$("#custom-search-input-database button").click(function(){
  $("#custom-search-input-database .glyphicon-refresh").show();
  $("#custom-search-input-database .glyphicon-search").hide();
  console.log({search: $('#search-db').val()});
  tree.tree('getTree').iterate(
    function(node, level) {
	    $el = $(node.element);
        $el.removeClass('highlight');
        return true;
    }
  );
  $.ajax({
   	type: "POST",
   	url: "serachDatabase",
   	data: JSON.stringify({'string': $('#search-db').val(), 'log': $('#logname').text()}, null, '\t'),
   	contentType: 'application/json;charset=UTF-8',
   	dataType: "json",
    success: function(msg){
	    for(var i = 0; i < msg.length; i++) {
		    console.log(msg[i]);
		    nodeId = msg[i];
		    node = tree.tree('getNodeById', nodeId);
		    console.log(tree.tree('getNodeById', nodeId));
		    
		    $el = $(node.element);
		    var classes = $el.attr('class');
			classes = 'highlight ' +classes;
			$el.attr('class', classes);
	    }
	    
	    $("#custom-search-input-database .glyphicon-refresh").hide();
		$("#custom-search-input-database .glyphicon-search").show();
		msgFound = msg.length > 1 ? "messages" : "message";
		$("#custom-search-input-database-results").html("<b>Result: "+msg.length+" "+msgFound+" found in database</b>")
    },
	error: function(){
	  console.log("failure");
	}
  });
});

$("#search-db").keypress(function(e){
    if(e.which == 13){//Enter key pressed
        $('#custom-search-input-database button').click();//Trigger search button click event
    }
});
});