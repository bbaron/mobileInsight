# mi.py

import sys, os, re, ast
from multiprocessing import Process
import unicodedata
import progressbar
from bson.code import Code
import logging

try:
	from mobile_insight.monitor import OfflineReplayer
	from mobile_insight.analyzer import LteRrcAnalyzer,WcdmaRrcAnalyzer, MsgLogger
except ImportError as e:
	pass

UPLOAD_FOLDER = "logs/"
LOG_DEBUG_FOLDER = "log_debug/"


def analyze_offline_log(logpath):
	"""
	Retrieve the plaintext from the log file
	Put the messages in the local mongoDB
	"""
	
	try:
		src = OfflineReplayer()
		# Load offline logs
		src.set_input_path(logpath)
		src.set_log(LOG_DEBUG_FOLDER, loglevel=logging.ERROR)
		
		# RRC analyzer
		lte_rrc_analyzer = LteRrcAnalyzer()
		lte_rrc_analyzer.set_log(LOG_DEBUG_FOLDER, loglevel=logging.ERROR)
		lte_rrc_analyzer.set_source(src) #bind with the monitor
	
		wcdma_rrc_analyzer = WcdmaRrcAnalyzer()
		wcdma_rrc_analyzer.set_log(LOG_DEBUG_FOLDER, loglevel=logging.ERROR)
		wcdma_rrc_analyzer.set_source(src) #bind with the monitor
	
		dumper = MsgLogger()  # Declare an analyzer
		dumper.set_log(LOG_DEBUG_FOLDER, loglevel=logging.ERROR)
		dumper.set_source(src) # Bind the analyzer to the monitor
		dumper.set_decoding(MsgLogger.JSON)  # decode the message as json
		dumper.set_dump_type(MsgLogger.FILE_ONLY)
		
		extension = os.path.splitext(logpath)[1]
		name 	  = os.path.splitext(logpath)[0]
		path      = name+".txt"
	
		print(logpath + " | save in "+path)
		dumper.save_decoded_msg_as(path)
		p = Process(target=src.run)
		p.start()
		p.join()
		# src.run()
		print("done running!")	
	except Exception as e:
		print("Exception caught: ", e)
		return None
		
	return path


def get_db():
	from pymongo import MongoClient
	client = MongoClient('mongodb:27017')
	return client.local


def get_remote_db():
	from pymongo import MongoClient
	client = MongoClient('82.236.68.108:27017')
	return client.local


def get_logs(db):
	l = []
	cur = db.local.logs.find({})
	for doc in cur:
		l.append({
			"date": doc['day']+"/"+doc['month']+"/"+doc['year'],
	        "time": doc['hour']+":"+doc['min']+":"+doc['sec'],
	        "phone": doc['phone'],
	        "carrier": doc['carrier'],
	        "messages": len(doc['messages']),
	        "name": doc['name']
	    })
	return l

def get_messages(db, name):
	d = db.local.logs.find_one({'name': name})
	l = []
	for mes in d['messages']:
		l.append({
			'name': mes['count'],
			'id': mes['count'],
			'type': mes['type_id'],
			'ts': mes['timestamp'],
			"load_on_demand": True
		})
	return l


count_msg = 0
def message_attributes_to_dict(message, count):
	global count_msg

	l = []
	for k in sorted(message, key=lambda v: v.upper()):
		v = message[k]
		if not hasattr(v, '__iter__') and k != '_id':
			l.append({
				'name': "%s" % (k),
				'value': "%s" % (v),
				'id': "%s-%s" % (count, count_msg)
			})
			count_msg += 1

	# add the message contents ("Msg")
	if 'Msg' in message:
		count_msg += 1
		if 'proto' in message['Msg']['msg']['packet']:
			l.append({
				'name': "message",
				'id': "%s-%s" % (count, count_msg-1),
				'children': proto_to_dict([message['Msg']['msg']['packet']['proto'][3]], count)
			})
		else:
			children = proto_to_dict([message['Msg']['msg']['packet'][0]['proto'][3]], count)
			for i in range(1,len(message['Msg']['msg']['packet'])):
				children += proto_to_dict([message['Msg']['msg']['packet'][i]['proto'][3]], count)
				
			l.append({
				'name': "message",
				'id': "%s-%s" % (count, count_msg-1),
				'children': children
			})

	return l


def proto_to_dict(proto, count):
	global count_msg
	l = []
	for p in proto:
# 		print(p['@name'])
		if p['@name'] == 'fake-field-wrapper' or p['@name'] == '':
			p = p['field']
			if isinstance(p, list):
				return proto_to_dict(p, count)

		msg = {'name': p['@name'], 'id': "%s-%s" % (count, count_msg)}
		count_msg += 1
		if '@showname' in p:
			msg['value'] = p['@showname']

		if 'field' in p:
			if isinstance(p['field'], dict):
				p['field'] = [p['field']]
			# recusive call the function 
			msg['children'] = proto_to_dict(p['field'], count)

		l.append(msg)

	return l


def get_ids(d, s = set()):
	flag = True
	if isinstance(d, list):
		for e in d:
			flag = flag & get_ids(e, s)[0]

	elif isinstance(d, dict):
		if 'children' in d:
			flag = flag & get_ids(d['children'], s)[0]
		
		if d['id'] in s:
			print("error: dupliacted id %s" % d['id'])
			flag = False
		else:
			s.add(d['id'])

	return flag, len(s)


def get_message_content(db, name, count):
	d = db.local.messages.find_one({'message_id': {'log': name, 'count': int(count)}})
	d1 = message_attributes_to_dict(d, count)
# 	print(get_ids(d1, set()))

	return d1


def add_log_db(db, log):
	pattern = r"^[^\_]+\_[^\_]+\_(\d{4})(\d{2})(\d{2})\_(\d{2})(\d{2})(\d{2})\_([^\_]+)\_([^\_\.]+)"
	h,t = os.path.split(log)
	name = os.path.splitext(t)[0]

	d = db.local.logs.find_one({'name':name})
	if d and len(d['messages']) > 0:
		print(log + ' already in DB')
		return

	m = re.search(pattern, t)
	document = {"name":    name, 
				"year":    m.group(1),
				"month":   m.group(2),
				"day":     m.group(3),
				"hour":    m.group(4),
				"min":     m.group(5),
				"sec":     m.group(6),
				"phone":   m.group(7),
				"carrier": m.group(8),
				"messages": []}
	count = 1
	num_lines = sum(1 for line in open(log, 'r'))
	# bar = progressbar.ProgressBar(maxval=num_lines)
	with open(log, 'r') as f:
		for message in f:
			message_dict = {}
			try:
				message_dict = ast.literal_eval(message)
			except:
				# print("SyntaxError -- pass")
				continue
				
			message_dict["message_id"] = {
				"log": name,
				"count": count
			}
			_id = db.local.messages.insert_one(message_dict).inserted_id
			d = {
				"type_id": message_dict["type_id"],
				"timestamp": message_dict["timestamp"],
				"_id": _id,
				"count": count
			}
			document["messages"].append(d)

			count += 1
			# bar.update(count)
			if num_lines == count:
				break

	db.local.logs.insert_one(document)


def get_message_fields(db):
	d = {}
	count = 0
	cur = db.local.messages.find({})
	for m in cur:
		count += 1
		for k,v in m.items():
			if k not in d:
				d[k] = 0
			d[k] += 1


def send_mail(Message, files):
	msg = Message("New logs to process", 
				  sender = 'Mobile Insight | Logs <benji.baron@gmail.com>',
				  recipients = ['benjamin.baron@me.com'])

	msg.html = "<h2>New logs added:</h2><ul>"
	for f in files['files']:
		msg.html += "<li>"+f['name']+"</li>"
	msg.html += "</ul>"
	return msg


def process_new_logs(db, path):
# 	print("process log "+path)
	p = Process(target=analyze_offline_log, args=(path,))
	p.start()
	p.join()

	logpath = os.path.splitext(path)[0] + ".txt"

	# put the txt in the mongoDB database
	add_log_db(db, logpath)


def exists_in_db(db, name):
	d = db.local.logs.find_one({'name': name})
	if d and len(d['messages']) > 0:
		return True
	return False

def get_database_results(db, search):
# 	print(search)/
	s = search['string']
	log = search['log']
# 	print(s+" "+log)
	map = Code("function() {"
		       "	var recursiveSearch = function(d, str) {"
		       "	    var findVal = str.toLowerCase();"
		       "	    function inspectObj(doc) {"
		       "	      return Object.keys(doc).some(function(key) {"
		       "	        if ( typeof(doc[key]) == 'object' ) {"
		       "	          return inspectObj(doc[key]);"
		       "	        } else {"
		       "	          if (typeof(doc[key]) == 'string' && doc[key].toLowerCase().indexOf(findVal) >= 0) {"
		       "	              return 1;"
		       "	          } else {"
		       "	              return 0;"
		       "	          }"
		       "	        }"
		       "	      });"
		       "	    }"
		       "	    return inspectObj(d);"
		       "	};"
		       "	emit(this.message_id.count, recursiveSearch(this, '"+s+"'));"
		       "}")
	reduce = Code("function(key, values) {return Array.sum( values );}")
	result = db.local.messages.map_reduce(map, reduce, "log_search", query={"message_id.log": log})
	l = [int(e['_id']) for e in result.find({'value': True})]
	return l


def main():
	import pysftp
	# connect to MongoDB
	db = get_db()
	process_path = set()

	with pysftp.Connection('82.224.194.132', username='alarm', password='alarm', port=2222) as sftp:
		with sftp.cd('mobileInsight/logs/'):
			files = sftp.listdir()
			done_files = set()
			for f in files:
				extension = os.path.splitext(f)[1]
				name 	  = os.path.splitext(f)[0]
				if extension == '.txt' or exists_in_db(db, name):
					done_files.add(name)
					continue
			todo_files = set()
			for f in files:
				extension = os.path.splitext(f)[1]
				name 	  = os.path.splitext(f)[0]
				if name not in done_files:
					todo_files.add(f)

			for f in todo_files:
				print("### process log file "+f)
				# get log file from server
				sftp.get(f)
				# move log file to log folder
				path = os.path.join(UPLOAD_FOLDER, f)
				# print(path)
				os.rename(f, path)
				process_path.add(path)

	for path in process_path:
		# process the log file
		path = path.encode('ascii','ignore')
		process_new_logs(db, path)

if __name__ == '__main__':
# 	main()
#	logpath = 'logs/diag_log_20170217_183034_359864069350821_Sony-D5803_OrangeF.mi2log'
	logpath = sys.argv[1]
	analyze_offline_log(logpath)
