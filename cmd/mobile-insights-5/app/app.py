from flask import Flask, render_template, request
from flask_mail import Mail, Message
from werkzeug.utils import secure_filename
import os, sys, json
import time

from mi import *

UPLOAD_FOLDER = "logs/"

# create the application object
app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['MAIL_SERVER']='smtp.gmail.com'
app.config['MAIL_PORT'] = 465
app.config['MAIL_USERNAME'] = 'benji.baron@gmail.com'
app.config['MAIL_PASSWORD'] = 'dpgyuaqrldwolyyc'
app.config['MAIL_USE_TLS'] = False
app.config['MAIL_USE_SSL'] = True
mail = Mail(app)

db = get_db()

# use decorators to link the function to a url
@app.route('/')
def home():
    return render_template("loglist.html")  # return a string

@app.route('/welcome')
def welcome():
    return render_template('welcome.html')  # render a template

@app.route('/showLog', methods = ['GET'])
def showLog():
    if request.method == 'GET':
        name = request.args.get('name')
    return render_template('msglist.html', name = name)  # render a template


@app.route('/getLogs')
def getLogs():
    return json.dumps(get_logs(db))
	
@app.route('/serachDatabase', methods = ['GET', 'POST'])
def searchDatabase():
    return json.dumps(get_database_results(db, request.json))


@app.route('/getMessages', methods = ['GET', 'POST'])
def getMessages():
    if request.method == 'GET':
        count = request.args.get('node')
        name = request.args.get('name')
        if not count:
            return json.dumps(get_messages(db, name))
        return json.dumps(get_message_content(db, name, count))


@app.route('/uploader', methods = ['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        files = request.files.getlist("files[]")
        print(files)
        d = {"files":[]}
        for f in files:
            filename = secure_filename(f.filename)
            # save the file. 
            path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            if os.path.isfile(path):
                continue

            # save file in UPLOAD_FOLDER and send mail
            f.save(path)
            process_new_logs(db, path)
            d["files"].append({"name": filename})

# 		if d["files"]:
# 			msg = send_mail(Message, d)
# 			mail.send(msg)

        return json.dumps(d)

# start the server with the 'run()' method
if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=80, threaded=True)
